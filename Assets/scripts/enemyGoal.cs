﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemyGoal : MonoBehaviour {

	// Vastustajien päämäärä jossa vähennetään pelaajalta elämiä ja tapetaan vihut
	GameObject playerHealth;
	GameObject enemyHealth;

	void Start () {
		playerHealth = GameObject.FindGameObjectWithTag ("Player");
		enemyHealth = transform.parent.gameObject;
	}

	void OnTriggerEnter(Collider other) {
		if (other.tag == "EnemyGoal") {
			playerHealth.GetComponent<PlayerHealth> ().takeDMG (1);
			enemyHealth.GetComponent<EnemyHealth> ().takeDMG (30000000);
		}
	}
}
