﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAttack : MonoBehaviour {

	public int baseDmg = 1;
	public float timeBetween;

	GameObject player;
	PlayerHealth playerHealth;
	bool playerInRange;
	float timer;

	// Use this for initialization
	void Start () {
		player = GameObject.FindGameObjectWithTag ("Player");
		playerHealth = player.GetComponent<PlayerHealth> ();
	}
	//Jos trigger collider osuu pelaajaan ni laitetaan pelaajanrange booli true
	void OnTriggerEnter (Collider other) {
		if (other.gameObject == player) {
			playerInRange = true;
		}
	}
	void OnTriggerExit (Collider other) {
		if (other.gameObject == player) {
			playerInRange = false;
		}
	}
	
	// Update is called once per frame
	//isketään pelaajaan tietyn aikavälein jos pelaaja on rangella
	void Update () {
		timer += Time.deltaTime;

		if (timer >= timeBetween && playerInRange) {
			Attack();

		}
		if (playerHealth.currentHealth <= 0) {
			
		}
	}
	void Attack() {
		timer = 0f;

		if (playerHealth.currentHealth > 0) {
			playerHealth.takeDMG (baseDmg);
		}
	}
}
