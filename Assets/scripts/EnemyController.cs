﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
[RequireComponent (typeof(NavMeshAgent))]
public class EnemyController : MonoBehaviour {

	public float minDistance = 2;
	public float baseDmg = 1;
	public float speed = 2;

	GameObject goal;

	NavMeshAgent pathfinder;
	Transform goalPos;
	Transform palyer;
	float distance;
	float extraRotSpeed = 5;
	EnemyHealth enemyHealth;

	// Use this for initialization
	void Start () {
		enemyHealth = GetComponent<EnemyHealth> ();
		pathfinder = GetComponent<NavMeshAgent> ();
		goal = GameObject.FindGameObjectWithTag ("EnemyGoal");
		goalPos = goal.transform;
		palyer = GameObject.FindGameObjectWithTag ("Player").transform;
		distance = Vector3.Distance (transform.position, palyer.position);
		GetComponent<NavMeshAgent> ().enabled = true;
		StartCoroutine (UpdatePath ());
	}
	
	// Update is called once per frame
	void Update () {
		if(!enemyHealth.isDead)
			extraRotation ();
	}
	//päivitettän rotaationi nopeammin

	void extraRotation() {
		Vector3 lookrotation = pathfinder.steeringTarget - transform.position;
		transform.rotation = Quaternion.Slerp(transform.rotation,Quaternion.LookRotation(lookrotation), extraRotSpeed * Time.deltaTime);

	}
	//käytetää ienumeraattoria polun päivityksee ettei käytä niipaljoo resurssei
	IEnumerator UpdatePath() {
		float refreshRate = .25f;

		while (!enemyHealth.isDead) {
			distance = Vector3.Distance (transform.position, palyer.position);
			if (distance < minDistance) {
				Vector3 palyerPos = new Vector3 (palyer.position.x, 0, palyer.position.z);
				pathfinder.SetDestination (palyerPos);
			} else {
				Vector3 goalDest = new Vector3 (goalPos.position.x, 0, goalPos.position.z);
				pathfinder.SetDestination (goalDest);
			}
			yield return new WaitForSeconds (refreshRate);
		}
	}
}
