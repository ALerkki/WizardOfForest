﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
//Menu nappuloiden koodia ja metodeita
public class MenuController : MonoBehaviour {

	public GameObject HelpSc;
	public GameObject StartSc;

	void Start() {
		showStart ();
	}

	public void LoadLevel() {
		SceneManager.LoadScene ("Game", LoadSceneMode.Single);
		print ("Test");
	}

	public void showHelp() {
		HelpSc.SetActive (true);
		StartSc.SetActive (false);
	}

	public void showStart() {
		HelpSc.SetActive (false);
		StartSc.SetActive (true);
	}
}
