﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Game : MonoBehaviour {


	//Components
	public GameObject enemy;
	public GameObject spawnPoints;
	PlayerHealth playerHealth;


	//Hidden Public Vars
	[HideInInspector]
	public int score, enemyCount = 5, wave = 1,testCount;


	//Vars
	public int difficulty;
	private float level;

	// Update is called once per frame
	void LateUpdate () {
		enemyCount = GameObject.Find ("Enemies").transform.childCount;
		if (enemyCount == 0) {
			wave += 1;
			print (wave);
			WaveSpawn ((5+wave));
		}
	}

	//public methdos
	public void addScore() {
		score += 1;
	}

	//private methods
	void WaveSpawn(int amount) {
		int i;
		for (i = 0; i < amount; i++) {
			spawn ();
		}

	}
	//spawni metodi jossa spawnataan vihollinen random spawnpointtiin
	void spawn() {
		Transform pos = spawnPoints.transform.GetChild ((int)Random.Range (0, spawnPoints.transform.childCount));
		GameObject newEnemy = Instantiate (enemy, pos.position, pos.rotation) as GameObject;
		newEnemy.transform.parent = GameObject.FindGameObjectWithTag ("EnemyBundle").transform;
	}

	public void gameOver() {
		SceneManager.LoadScene ("Menu");
	}
}
