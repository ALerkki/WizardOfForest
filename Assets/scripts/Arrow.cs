﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arrow : MonoBehaviour {

	public int dmg = 1;
	public int lifetime;

	GameObject enemy;
	EnemyHealth enemyHealth;
	private Rigidbody rb;
	Game game;

	void Start () {
		
		game = GameObject.FindGameObjectWithTag ("GameController").GetComponent<Game> ();

		rb = GetComponent<Rigidbody> ();
		Destroy (gameObject, lifetime);

	}
	//Testataan collisionit joko levelin tai vastustajan kanssa
	void OnCollisionEnter(Collision other){
		if (other.gameObject.transform.IsChildOf(GameObject.Find("Level").transform)) {
			transform.parent = other.gameObject.transform;
			rb.Sleep ();
			gameObject.layer = 12;
			
		} else {
			Destroy (gameObject);

			if (other.gameObject.transform.IsChildOf (GameObject.Find ("Enemies").transform)) {
				other.gameObject.GetComponent<EnemyHealth> ().takeDMG (dmg);
				game.addScore ();
			}
		}

	}


	void Update () {
		
		if (rb.IsSleeping ()) {
		} else {
			transform.rotation = Quaternion.LookRotation (rb.velocity);
		}
	}
}
