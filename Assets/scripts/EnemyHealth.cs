﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyHealth : MonoBehaviour {
	
	public int startHealth;
	public int currentHealth;
	public bool isDead;
	// Use this for initialization
	void Start () {
		currentHealth = startHealth;
		isDead = false;
	}
	//simppeli ota dmg metodi jossa ekas if lausees tarkistetaan onko kyseinen vihu edes elossa jos ei ni sammutetaa
	//ja sit vähennetää hp ja sit jos hp iskeee 0 ni dööd
	public void takeDMG(int amount) {
		if (isDead)
			return;
		currentHealth -= amount;
		if (currentHealth <= 0) {
			Death ();
		}
	}
	void Death() {
		isDead = true;
		GetComponent<NavMeshAgent> ().enabled = false;
		Destroy (this.gameObject, 2f);
	}
}
