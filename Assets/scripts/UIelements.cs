﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/*

Peli hudi jos löytyy hp ja löytyy vihollisten määrä score ja wave numero ja ne päivityy siel updates

*/

public class UIelements : MonoBehaviour {

	public Text healthUI;
	public Text scoreUI;
	public Text waveUI;
	public Text enemyCountUI;
	PlayerHealth playerHealth;
	Game game;

	// Use this for initialization
	void Start () {
		playerHealth = GameObject.FindGameObjectWithTag ("Player").GetComponent<PlayerHealth> ();
		game = GameObject.FindGameObjectWithTag ("GameController").GetComponent<Game> ();

	}
	
	// Update is called once per frame
	void Update () {
		healthUI.text = playerHealth.currentHealth.ToString ();
		scoreUI.text = game.score.ToString ();
		waveUI.text = game.wave.ToString ();
		enemyCountUI.text = game.enemyCount.ToString ();
	}
}
