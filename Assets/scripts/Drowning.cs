﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Drowning : MonoBehaviour {
	
	PlayerHealth playerHealth;
	// Use this for initialization
	void Start () {
		playerHealth = GetComponent<PlayerHealth> ();
	}

	//jos pelaaja tippuu veteen niin kuolee
	void OnTriggerEnter(Collider other) {
		if (other.name == "Water") {
			playerHealth.takeDMG (9001);
		}
	}
}
