﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//requires charactercontroller on object (if not present will create one)
[RequireComponent (typeof (CharacterController))]
public class PlayerController : MonoBehaviour {

	//CharVars
	public float rotationSpeed = 450;
	public float walkSpeed = 5;
	public float runSpeed = 10;


	//SysVars
	private Quaternion targetRotation;
	private float lockPos = 0;

	//Components
	private CharacterController controller;
	public Gun gun;
	// Use this for initialization
	void Start () {
		controller = GetComponent<CharacterController> ();
	}
	
	// Update is called once per frame
	void Update () {
		//aim with mouse
		AimWithMouse (); 
		transform.rotation = Quaternion.Euler (lockPos, transform.rotation.eulerAngles.y, lockPos); //lock x,z rotation to keep char level

		//walk and run
		MoveWithWASD();

		//shooting
		if (Input.GetButtonDown ("Shoot")) {
			gun.Shoot();
		}
	}

	//mouse aim
	void AimWithMouse() {
		Vector3 mousePos = Input.mousePosition;
		var ray = Camera.main.ScreenPointToRay (mousePos);
		RaycastHit hit;

		if (Physics.Raycast (ray, out hit, 100f)) {
			transform.LookAt (hit.point);
			var playerT = GameObject.FindWithTag ("Player").transform;
			Debug.DrawLine (playerT.position, hit.point);
		}
	}

	//movement
	void MoveWithWASD() {
		Vector3 input = new Vector3 (Input.GetAxisRaw ("Horizontal"), 0, Input.GetAxisRaw ("Vertical"));

		Vector3 motion = input;
		//eka laitetaa inputit motionii ja sit testataa conditionalil et jos tulee enemmä ku 2 inputtia samaa aikaa ni laitetaa 0.7
		//ku liikutaa viistoo ja jos suoraa ni laitetaa "nopeudeks" 1
		motion *= (Mathf.Abs(input.x) == 1 && Mathf.Abs(input.z) == 1) ? .7f:1;
		motion *= (Input.GetButton ("Run")) ? runSpeed : walkSpeed; //shiftistä juoksee nopeempaa pieni easteregg atm
		motion += Vector3.up * -8; //laitetaa ukkelille gravityä
		//controlleri liikutetaan inputtia kohti
		controller.Move (motion * Time.deltaTime);
	}

}
