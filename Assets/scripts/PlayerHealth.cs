﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerHealth : MonoBehaviour {

	//CharVars
	public int startHealth = 20;
	public int currentHealth;
	[HideInInspector]
	public bool isDead;

	//playerhealth iha sama ku enemyhealth mut gameover metodi joka heittää takas menuu
	//jos pelaaja kuoleee

	//Components
	PlayerController playerController;
	// Use this for initialization
	void Start () {
		currentHealth = startHealth;
		isDead = false;
		playerController = GetComponent<PlayerController> ();
	}

	// Update is called once per frame
	void Update () {
		
	}
	public void takeDMG(int amount) {
		currentHealth -= amount;

		if (currentHealth <= 0 && !isDead) {
			Death ();
		}
	}
	void Death() {
		isDead = true;
		playerController.enabled = false;
		Invoke ("gameOver", 3);
	}
	void gameOver() {
		SceneManager.LoadScene ("Menu");
	}
}
