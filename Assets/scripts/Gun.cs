﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour {

	public Transform barrel;
	public GameObject projectile;
	private float secondsBetween = 2;
	private float nextPossibleShootTime;
	private float arrowSpeed = 30f;

	public float shotDist = 30; //Dbug

	public void Shoot() {

		if (CanShoot ()) {
			//Debug ampumiskoodi
			Ray ray = new Ray (barrel.position, barrel.forward);
			RaycastHit hit;
			if (Physics.Raycast (ray, out hit, shotDist)) {
				shotDist = hit.distance;
			}
			Debug.DrawRay (ray.origin, ray.direction * shotDist, Color.blue, 1);

			//"Completed shooting" spawnataan arroweita tikun päästä
			GameObject newArrow = Instantiate (projectile, barrel.position, barrel.rotation) as GameObject;
			newArrow.transform.parent = GameObject.FindGameObjectWithTag ("ArrowBundle").transform;
			Rigidbody aRb = newArrow.GetComponent<Rigidbody> ();
			aRb.velocity = barrel.transform.forward*arrowSpeed;
			nextPossibleShootTime = Time.time + secondsBetween;
		}



	}

	//Check if you are able to shoot has there been enough time between
	private bool CanShoot() {
		bool canShoot = true;

		if (Time.time < nextPossibleShootTime) {
			canShoot = false;
		}
		return canShoot;
	}
}
